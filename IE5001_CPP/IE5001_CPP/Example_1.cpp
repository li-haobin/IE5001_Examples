#include "stdafx.h"
#include "Example_1.h"


Example_1::Example_1()
{
}


Example_1::~Example_1()
{
}

void Example_1::RunCplex()
{
	IloEnv env;
	IloModel model(env);

	IloIntVarArray x(env, 2, 0, 20); // Decision Variables
	model.add(IloMaximize(env, 5000 * x[0] + 4000 * x[1])); // objective

	model.add(10 * x[0] + 15 * x[1] <= 150);	// constraint 1
	model.add(20 * x[0] + 10 * x[1] <= 160);	// constraint 2
	model.add(30 * x[0] + 10 * x[1] >= 135);	// constraint 3
	model.add(x[0] - 3 * x[1] <= 0);			// constraint 4
	model.add(x[0] + x[1] >= 5);				// constraint 5

	IloCplex cplex(model);
	cplex.solve();

	system("CLS");
	std::cout << "Decision Variables:\t" 
		<< cplex.getValue(x[0]) << ",\t" 
		<< cplex.getValue(x[1]) 
		<< std::endl;
	std::cout << "Objective:\t" << cplex.getObjValue() << std::endl;

	env.end();
}
