#include "stdafx.h"
#include "Example_6.h"


Example_6::Example_6()
{
}


Example_6::~Example_6()
{
}

void Example_6::RunCplex()
{
	IloEnv env;
	IloModel model(env);

	IloNumVarArray x(env, 4, 0, IloInfinity); // Decision Variables
	model.add(IloMinimize(env, 800 * x[0] + 400 * x[1] + 600 * x[2] + 500 * x[3])); // objective

	model.add(10 * x[0] + 3 * x[1] + 8 * x[2] + 2 * x[3] >= 5);			// constraint 1
	model.add(90 * x[0] + 150 * x[1] + 75 * x[2] + 175 * x[3] >= 100);	// constraint 2
	model.add(45 * x[0] + 25 * x[1] + 20 * x[2] + 37 * x[3] >= 30);		// constraint 3
	model.add(IloSum(x) == 1);											// constraint 4
	
	IloCplex cplex(model);
	cplex.solve();

	system("CLS");
	std::cout << "Decision Variables:\t" 
		<< cplex.getValue(x[0]) << ",\t" 
		<< cplex.getValue(x[1]) << ",\t"
		<< cplex.getValue(x[2]) << ",\t"
		<< cplex.getValue(x[3]) 
		<< std::endl;
	std::cout << "Objective:\t" << cplex.getObjValue() << std::endl;

	env.end();
}
