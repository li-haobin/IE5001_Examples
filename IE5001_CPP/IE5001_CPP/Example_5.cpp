#include "stdafx.h"
#include "Example_5.h"


Example_5::Example_5()
{
}


Example_5::~Example_5()
{
}

void Example_5::RunCplex()
{
	IloEnv env;
	IloModel model(env);

	IloNumVarArray x(env, 6, 0, 1); // Decision Variables
	model.add(IloMinimize(env, 8 * x[0] + 10 * x[1] + 7 * x[2] + 6 * x[3] + 11 * x[4] + 9 * x[5])); // objective

	model.add(12 * x[0] + 9 * x[1] + 25 * x[2] + 20 * x[3] + 17 * x[4] + 13 * x[5] >= 60);		// constraint 1
	model.add(35 * x[0] + 42 * x[1] + 18 * x[2] + 31 * x[3] + 56 * x[4] + 49 * x[5] >= 150);	// constraint 2
	model.add(37 * x[0] + 53 * x[1] + 28 * x[2] + 24 * x[3] + 29 * x[4] + 20 * x[5] >= 125);	// constraint 3

	IloCplex cplex(model);
	cplex.solve();

	system("CLS");
	std::cout << "Decision Variables:\t"
		<< cplex.getValue(x[0]) << ",\t"
		<< cplex.getValue(x[1]) << ",\t"
		<< cplex.getValue(x[2]) << ",\t"
		<< cplex.getValue(x[3]) << ",\t"
		<< cplex.getValue(x[4]) << ",\t"
		<< cplex.getValue(x[5])
		<< std::endl;
	std::cout << "Objective:\t" << cplex.getObjValue() << std::endl;

	env.end();
}
