// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <ilcplex/ilocplex.h>
#include "Example_1.h"
#include "Example_2.h"
#include "Example_3.h"
#include "Example_4.h"
#include "Example_5.h"
#include "Example_6.h"
#include "Example_7.h"
#include "Example_8.h"
#include "Example_9.h"

// TODO: reference additional headers your program requires here
