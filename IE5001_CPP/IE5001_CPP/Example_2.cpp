#include "stdafx.h"
#include "Example_2.h"


Example_2::Example_2()
{
}


Example_2::~Example_2()
{
}

void Example_2::RunCplex()
{
	IloEnv env;
	IloModel model(env);

	IloNumVarArray x(env, 2, 0, IloInfinity); // Decision Variables
	model.add(IloMaximize(env, 5 * x[0] + 4 * x[1])); // objective

	model.add(6 * x[0] + 4 * x[1] <= 24);	// constraint 1
	model.add(x[0] + 2 * x[1] <= 6);		// constraint 2
	model.add(x[1] <= 2);					// constraint 3
	model.add(- x[0] + x[1] <= 1);			// constraint 4

	IloCplex cplex(model);
	cplex.solve();

	system("CLS");
	std::cout << "Decision Variables:\t"
		<< cplex.getValue(x[0]) << ",\t"
		<< cplex.getValue(x[1])
		<< std::endl;
	std::cout << "Objective:\t" << cplex.getObjValue() << std::endl;

	env.end();
}
