#include "stdafx.h"
#include "Example_8.h"


Example_8::Example_8()
{
}


Example_8::~Example_8()
{
}

void Example_8::RunCplex()
{
	IloEnv env;
	IloModel model(env);

	IloNumVarArray x(env, 5, 0, IloInfinity); // Decision Variables
	IloNumExpr expectedInterestReturn = .14 * .9 * x[0] + .13 * .93 * x[1] + .12 * .97 * x[2] + .125 * .95 * x[3] + .1 * .98 * x[4];
	IloNumExpr expectedBadDebt = .1 * x[0] + .07 * x[1] + .03 * x[2] + .05 * x[3] + .02 * x[4];
	model.add(IloMaximize(env, expectedInterestReturn - expectedBadDebt)); // objective	

	model.add(IloSum(x) <= 12);						// constraint 1	
	model.add(x[3] + x[4] >= .4 * 12);				// constraint 2
	model.add(x[2] >= .5 * (x[0] + x[1] + x[2]));	// constraint 3
	model.add(expectedBadDebt <= .04 * IloSum(x));	// constraint 4

	IloCplex cplex(model);
	cplex.solve();

	system("CLS");
	std::cout << "Decision Variables:\t"
		<< cplex.getValue(x[0]) << ",\t"
		<< cplex.getValue(x[1]) << ",\t"
		<< cplex.getValue(x[2]) << ",\t"
		<< cplex.getValue(x[3]) << ",\t"
		<< cplex.getValue(x[4]) 
		<< std::endl;
	std::cout << cplex.getValue(expectedInterestReturn) << "," << cplex.getValue(expectedBadDebt) << std::endl;
	std::cout << "Objective:\t" << cplex.getObjValue() << std::endl;

	env.end();
}
