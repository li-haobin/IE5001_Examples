#include "stdafx.h"
#include "Example_9.h"


Example_9::Example_9()
{
}


Example_9::~Example_9()
{
}

void Example_9::RunCplex()
{
	IloEnv env;
	IloModel model(env);

	IloIntVarArray x(env, 6, 0, 500); // Decision Variables
	model.add(IloMinimize(env, IloSum(x))); // objective


	model.add(4 * x[0] + 2 * x[1] + 1 * x[2] + 2 * x[3]                       >= 150);			// for 5ft
	model.add(		     1 * x[1] + 2 * x[2]            + 1 * x[4]            >= 200);			// for 7ft
	model.add(								   1 * x[3] + 1 * x[4] + 2 * x[5] >= 300);			// for 9ft
	
	IloCplex cplex(model);
	cplex.solve();

	system("CLS");
	std::cout << "Decision Variables:\t"
		<< cplex.getValue(x[0]) << ",\t"
		<< cplex.getValue(x[1]) << ",\t"
		<< cplex.getValue(x[2]) << ",\t"
		<< cplex.getValue(x[3]) << ",\t"
		<< cplex.getValue(x[4]) << ",\t"
		<< cplex.getValue(x[5]) 
		<< std::endl;
	std::cout << "Objective:\t" << cplex.getObjValue() << std::endl;

	env.end();
}
