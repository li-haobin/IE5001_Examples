#include "stdafx.h"
#include "Example_3.h"


Example_3::Example_3()
{
}


Example_3::~Example_3()
{
}

void Example_3::RunCplex()
{
	IloEnv env;
	IloModel model(env);

	IloNumVarArray x(env, 2, 0, IloInfinity); // Decision Variables
	model.add(IloMinimize(env, 0.3 * x[0] + 0.9 * x[1])); // objective

	model.add(x[0] + x[1] >= 800);				// constraint 1
	model.add(0.21 * x[0] - 0.3 * x[1] <= 0);	// constraint 2
	model.add(0.03 * x[0] - 0.01 * x[1] >= 0);	// constraint 3

	IloCplex cplex(model);
	cplex.solve();

	system("CLS");
	std::cout << "Decision Variables:\t" << cplex.getValue(x[0]) << ",\t" << cplex.getValue(x[1]) << std::endl;
	std::cout << "Objective:\t" << cplex.getObjValue() << std::endl;

	env.end();
}
