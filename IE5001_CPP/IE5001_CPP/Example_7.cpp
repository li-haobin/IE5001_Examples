#include "stdafx.h"
#include "Example_7.h"


Example_7::Example_7()
{
}


Example_7::~Example_7()
{
}

void Example_7::RunCplex()
{
	IloEnv env;
	IloModel model(env);

	IloIntVarArray x(env, 6, 0, 12); // Decision Variables
	model.add(IloMinimize(env, x[0] + x[1] + x[2] + x[3] + x[4] + x[5])); // objective

	model.add(x[0] + x[5] >= 5);		// midnight - 4am
	model.add(x[0] + x[1] >= 7);		// 4am - 8am
	model.add(x[1] + x[2] >= 15);		// 8am - noon
	model.add(x[2] + x[3] >= 7);		// noon - 4pm
	model.add(x[3] + x[4] >= 12);		// 4pm - 8pm
	model.add(x[4] + x[5] >= 9);		// 8pm - midnight

	IloCplex cplex(model);
	cplex.solve();

	system("CLS");
	std::cout << "Decision Variables:\t"
		<< cplex.getValue(x[0]) << ",\t"
		<< cplex.getValue(x[1]) << ",\t"
		<< cplex.getValue(x[2]) << ",\t"
		<< cplex.getValue(x[3]) << ",\t"
		<< cplex.getValue(x[4]) << ",\t"
		<< cplex.getValue(x[5])
		<< std::endl;
	std::cout << "Objective:\t" << cplex.getObjValue() << std::endl;

	env.end();
}
