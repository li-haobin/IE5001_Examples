#include "stdafx.h"
#include "Example_4.h"


Example_4::Example_4()
{
}


Example_4::~Example_4()
{
}

void Example_4::RunCplex()
{
	IloEnv env;
	IloModel model(env);

	IloNumVarArray x(env, 2, 0, IloInfinity); // Decision Variables
	model.add(IloMinimize(env, 0.4 * x[0] + 0.5 * x[1])); // objective

	model.add(0.3 * x[0] + 0.1 * x[1] <= 2.7);	// constraint 1
	model.add(0.5 * x[0] + 0.5 * x[1] == 6);	// constraint 2
	model.add(0.6 * x[0] + 0.4 * x[1] >= 6);	// constraint 3

	IloCplex cplex(model);
	cplex.solve();

	system("CLS");
	std::cout << "Decision Variables:\t"
		<< cplex.getValue(x[0]) << ",\t"
		<< cplex.getValue(x[1])
		<< std::endl;
	std::cout << "Objective:\t" << cplex.getObjValue() << std::endl;

	env.end();
}
