﻿using ILOG.Concert;
using ILOG.CPLEX;
using System;

namespace IE5001_CS
{
    class Example_3
    {
        // diet problem
        static public void RunCplex()
        {
            Cplex model = new Cplex();

            #region Decision Variables
            INumVar corn = model.NumVar(0, double.PositiveInfinity);
            INumVar soybeanMeal = model.NumVar(0, double.PositiveInfinity);
            #endregion

            #region Objective
            double costCorn = 0.3, costSoybeanMeal = 0.9;
            model.AddMinimize(model.Sum(model.Prod(costCorn, corn), model.Prod(costSoybeanMeal, soybeanMeal)));
            #endregion

            #region Constraints
            // minimum amount
            double minAmount = 800;
            model.AddGe(model.Sum(corn, soybeanMeal), minAmount);
            
            // Protein & Fiber Requirement
            double proteinPerCorn = 0.09, proteinPerSoybeanMeal = 0.6, proteinPerFeed = 0.3;
            double fiberPerCorn = 0.02, fiberPerSoybeanMeal = 0.06, fiberPerFeed = 0.05;
            model.AddGe(
                model.Sum(model.Prod(proteinPerCorn, corn), model.Prod(proteinPerSoybeanMeal, soybeanMeal)), 
                model.Prod(proteinPerFeed, model.Sum(corn, soybeanMeal)));
            model.AddLe(
               model.Sum(model.Prod(fiberPerCorn, corn), model.Prod(fiberPerSoybeanMeal, soybeanMeal)),
               model.Prod(fiberPerFeed, model.Sum(corn, soybeanMeal)));
            #endregion

            if (model.Solve())
            {
                Console.Clear();
                Console.WriteLine("Min Cost = ${0}", model.GetObjValue());
                Console.WriteLine("Corn = {0} lb, Soybean Meal = {1} lb", model.GetValue(corn), model.GetValue(soybeanMeal));
            }
            else
            {
                Console.WriteLine("Failed to optimize LP.");
            }
        }
    }
}
