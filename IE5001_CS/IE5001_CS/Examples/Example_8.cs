﻿using ILOG.Concert;
using ILOG.CPLEX;
using System;
using System.Linq;

namespace IE5001_CS
{
    class Example_8
    {
        static public void RunCplex()
        {
            Cplex model = new Cplex();
            INumVar[] amounts = model.NumVarArray(5, 0.0, double.PositiveInfinity); // decision variables     

            //model.AddMaximize(model.ScalProd(new double[] { .026, .0509, .0864, .06875, .078 }, amounts)); // objective         
            //model.AddLe(model.ScalProd(new double[] { 1, 1, 1, 1, 1 }, amounts), 12); // max fund
            //model.AddGe(model.ScalProd(new double[] { 0, 0, 0, 1, 1 }, amounts), 4.8); // for farm & commercial loan
            //model.AddLe(model.ScalProd(new double[] { .5, .5, -.5, 0, 0 }, amounts), 0); // for home loan
            //model.AddLe(model.ScalProd(new double[] { .06, .03, -.01, .01, -.02 }, amounts), 0); // bad debt rate

            INumVar personal = amounts[0], car = amounts[1], home = amounts[2], farm = amounts[3], commercial = amounts[4];
            double[] interestRates = new double[] { .140, .130, .120, .125, .100 };
            double[] badDebtProbabilities = new double[] { .10, .07, .03, .05, .02 };

            // objective
            int[] indices = Enumerable.Range(0, 5).ToArray();
            INumExpr[] expectedInterestReturn = indices.Select(i => model.Prod(interestRates[i] * (1.0 - badDebtProbabilities[i]), amounts[i])).ToArray();
            INumExpr[] expectedBadDebts = indices.Select(i => model.Prod(-badDebtProbabilities[i], amounts[i])).ToArray();
            model.AddMaximize(model.Sum(model.Sum(expectedInterestReturn), model.Sum(expectedBadDebts)));

            // constraints
            model.AddLe(model.Sum(amounts), 12); // max fund
            model.AddGe(model.Sum(farm, commercial), 12 * 0.4); // for farm & commercial loan
            model.AddGe(home, model.Prod(model.Sum(new INumExpr[] { personal, car, home }), 0.5)); // for home loan
            model.AddLe(model.ScalProd(badDebtProbabilities, amounts), model.Prod(model.Sum(amounts), .04)); // bad debt rate
            
            if (model.Solve())
            {
                Console.Clear();
                Console.WriteLine("Expected Net Return = {0}", model.GetObjValue());
                var decisionValues = model.GetValues(amounts);
                Console.WriteLine("Decisions = {0}, {1}, {2}, {3}, {4}",
                    decisionValues[0], decisionValues[1], decisionValues[2], decisionValues[3], decisionValues[4]);
            }
            else
            {
                Console.WriteLine("Failed to optimize LP.");
            }
        }
    }
}
