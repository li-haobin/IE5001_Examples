﻿using ILOG.Concert;
using ILOG.CPLEX;
using System;

namespace IE5001_CS
{
    // Protrac's Problem
    class Example_1
    {
        static public void RunCplex()
        {
            Cplex model = new Cplex();

            #region Decision Variables
            INumVar nE9 = model.NumVar(0, double.PositiveInfinity, NumVarType.Int);
            INumVar nF9 = model.NumVar(0, double.PositiveInfinity, NumVarType.Int);
            #endregion

            #region Objective
            double profitPerE9 = 5000, profitPerF9 = 4000;
            model.AddMaximize(model.Sum(model.Prod(profitPerE9, nE9), model.Prod(profitPerF9, nF9)));
            #endregion

            #region Constraints
            // time capacity in department A & B
            double hrsAvailableDeptA = 150, hrsPerE9inDeptA = 10, hrsPerF9inDeptA = 15;
            double hrsAvailableDeptB = 160, hrsPerE9inDeptB = 20, hrsPerF9inDeptB = 10;
            model.AddLe(model.Sum(model.Prod(hrsPerE9inDeptA, nE9), model.Prod(hrsPerF9inDeptA, nF9)), hrsAvailableDeptA);
            model.AddLe(model.Sum(model.Prod(hrsPerE9inDeptB, nE9), model.Prod(hrsPerF9inDeptB, nF9)), hrsAvailableDeptB);

            // minimum testing hrs
            double testingHrsGoal = 150, testingHrsThreshold = 0.1;
            double testingHrsPerE9 = 30, testingHrsPerF9 = 10;
            model.AddGe(model.Sum(model.Prod(testingHrsPerE9, nE9), model.Prod(testingHrsPerF9, nF9)), testingHrsGoal * testingHrsThreshold);

            // at least one F9 for every three E9
            double nF9PerE9 = 1.0 / 3;
            model.AddGe(nF9, model.Prod(nE9, nF9PerE9));

            // satisfy the order from major dealer
            int totalAmountOrdered = 5;
            model.AddGe(model.Sum(nE9, nF9), totalAmountOrdered);
            #endregion

            if (model.Solve())
            {
                Console.Clear();
                Console.WriteLine("Max Profit = ${0}", model.GetObjValue());
                Console.WriteLine("# of E9 = {0}, # of F9 = {1}", model.GetValue(nE9), model.GetValue(nF9));
            }
            else
            {
                Console.WriteLine("Failed to optimize LP.");
            }
        }
    }
}
