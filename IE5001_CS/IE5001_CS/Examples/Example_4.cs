﻿using ILOG.Concert;
using ILOG.CPLEX;
using System;

namespace IE5001_CS
{
    // Radiation Therapy
    class Example_4
    {
        static public void RunCplex()
        {
            Cplex model = new Cplex();            
            INumVar[] beams = model.NumVarArray(2, 0, double.PositiveInfinity); // decision variables            
            model.AddMinimize(model.ScalProd(new double[] { 0.4, 0.5 }, beams)); // healthy anatomy, objective         
            model.AddLe(model.ScalProd(new double[] { 0.3, 0.1 }, beams), 2.7); // critical tissues, constraint
            model.AddEq(model.ScalProd(new double[] { 0.5, 0.5 }, beams), 6.0); // tumor region, constraint
            model.AddGe(model.ScalProd(new double[] { 0.6, 0.4 }, beams), 6.0); // center of tumor, contraint            

            if (model.Solve())
            {
                Console.Clear();
                Console.WriteLine("Healthy Anatomy = {0}", model.GetObjValue());
                Console.WriteLine("Beam 1 = {0}, Beam 2 = {1}", model.GetValue(beams[0]), model.GetValue(beams[1]));
            }
            else
            {
                Console.WriteLine("Failed to optimize LP.");
            }
        }
    }
}
