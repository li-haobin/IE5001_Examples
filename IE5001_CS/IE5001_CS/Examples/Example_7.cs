﻿using ILOG.Concert;
using ILOG.CPLEX;
using System;

namespace IE5001_CS
{
    class Example_7
    {
        static public void RunCplex()
        {
            Cplex model = new Cplex();
            INumVar[] decisions = model.NumVarArray(6, 0.0, double.PositiveInfinity, NumVarType.Int); // decision variables     
            model.AddMinimize(model.Sum(decisions)); //total number, objective         
            model.AddGe(model.Sum(decisions[0], decisions[5]), 5); // midnight - 4am
            model.AddGe(model.Sum(decisions[0], decisions[1]), 7); // 4am - 8am
            model.AddGe(model.Sum(decisions[1], decisions[2]), 15); // 8am - noon
            model.AddGe(model.Sum(decisions[2], decisions[3]), 7); // noon - 4pm
            model.AddGe(model.Sum(decisions[3], decisions[4]), 12); // 4pm - 8pm
            model.AddGe(model.Sum(decisions[4], decisions[5]), 9); // 8pm - midnight

            if (model.Solve())
            {
                Console.Clear();
                Console.WriteLine("Cost = {0}", model.GetObjValue());
                var decisionValues = model.GetValues(decisions);
                Console.WriteLine("Decisions = {0}, {1}, {2}, {3}, {4}, {5}",
                    decisionValues[0], decisionValues[1], decisionValues[2], decisionValues[3], decisionValues[4], decisionValues[5]);
            }
            else
            {
                Console.WriteLine("Failed to optimize LP.");
            }
        }
    }
}
