﻿using ILOG.Concert;
using ILOG.CPLEX;
using MCSolver;
using System;
using System.Linq;

namespace IE5001_CS
{
    class Example_2
    {
        // Reddy Mikks's Problem
        static public void RunCplex()
        {
            Cplex model = new Cplex();

            #region Decision Variables
            INumVar exteriorPaint = model.NumVar(0, double.PositiveInfinity);
            INumVar interiorPaint = model.NumVar(0, double.PositiveInfinity);
            #endregion

            #region Objective
            double profitExteriorPaint = 5, profitInteriorPaint = 4;
            model.AddMaximize(model.Sum(model.Prod(profitExteriorPaint, exteriorPaint), model.Prod(profitInteriorPaint, interiorPaint)));
            #endregion

            #region Constraints
            // material availability
            double m1ForExteriorPaint = 6, m1ForInteriorPaint = 4, m1Availability = 24;
            double m2ForExteriorPaint = 1, m2ForInteriorPaint = 2, m2Availability = 6;
            model.AddLe(model.Sum(model.Prod(m1ForExteriorPaint, exteriorPaint), model.Prod(m1ForInteriorPaint, interiorPaint)), m1Availability);
            model.AddLe(model.Sum(model.Prod(m2ForExteriorPaint, exteriorPaint), model.Prod(m2ForInteriorPaint, interiorPaint)), m2Availability);

            // maximum daily demand for interior paint
            double maxDemandForInteriorPaint = 2;
            model.AddLe(interiorPaint, maxDemandForInteriorPaint);

            // interior paint cannot exceed exterior paint by more than 1 ton
            model.AddLe(interiorPaint, model.Sum(exteriorPaint, 1));
            #endregion

            if (model.Solve())
            {
                Console.Clear();
                Console.WriteLine("Max Profit = ${0}k", model.GetObjValue());
                Console.WriteLine("Exterior Paint = {0} ton, Interior Paint = {1} ton", model.GetValue(exteriorPaint), model.GetValue(interiorPaint));
            }
            else
            {
                Console.WriteLine("Failed to optimize LP.");
            }
        }

        static public void RunMoCompass()
        {
            var moCompass = new MoCompass(2,
                new double[][] {
                    new double[] { 6, 4, 24 }, // M1 availability
                    new double[] { 1, 2, 6 }, // M2 availability
                    new double[] { 0, 1, 2 }, // limitation of demand of iterior pait
                    new double[] { -1, 1, 1 }, // Excess of daily production of interior paint over that of exterior paint
                    new double[] { -1, 0, 0 },
                    new double[] { 0, -1, 0 },
                },
                decisions => { return new double[] { -decisions[0] * 5 - decisions[1] * 4 }; }
                );
            moCompass.Run(30, 3000);
            Console.WriteLine("Max Profit = ${0}k", -moCompass.ParetoSet[0][2]);
            Console.WriteLine("Exterior Paint = {0} ton, Interior Paint = {1} ton", moCompass.ParetoSet[0][0], moCompass.ParetoSet[0][1]);
        }

        static public void RunMoCompass_LagrangianRelaxation()
        {
            var moCompass = new MoCompass(2,
               new double[][] {
                    new double[] { 0, 1, 2 }, // limitation of demand of iterior pait
                    new double[] { -1, 1, 1 }, // Excess of daily production of interior paint over that of exterior paint
                    new double[] { -1, 0, 0 },
                    new double[] { 0, -1, 0 },
               },
               decisions =>
               {
                   return new double[] {
                        -decisions[0] * 5 - decisions[1] * 4 +
                        Math.Max(0, decisions[0] * 6 + decisions[1] * 4 - 24) + // violation of M1 availability
                        Math.Max(0, decisions[0] * 1 + decisions[1] * 2 - 6) // violation of M2 availability
                   };
               });
            moCompass.Run(30, 3000);
            Console.WriteLine("Max Profit = ${0}k", -moCompass.ParetoSet[0][2]);
            Console.WriteLine("Exterior Paint = {0} ton, Interior Paint = {1} ton", moCompass.ParetoSet[0][0], moCompass.ParetoSet[0][1]);
        }

    }
}
