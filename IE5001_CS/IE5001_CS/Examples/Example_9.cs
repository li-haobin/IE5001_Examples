﻿using ILOG.Concert;
using ILOG.CPLEX;
using System;
using System.Linq;

namespace IE5001_CS
{
    class Example_9
    {
        static public void RunCplex()
        {
            Cplex model = new Cplex();
            INumVar[] nForPatterns = model.NumVarArray(6, 0.0, double.PositiveInfinity, NumVarType.Int); // decision variables     

            int nTypes = 3;
            int[][] patterns = new int[][] {
                new int[] { 4, 0, 0},
                new int[] { 2, 1, 0},
                new int[] { 1, 2, 0},
                new int[] { 2, 0, 1},
                new int[] { 0, 1, 1},
                new int[] { 0, 0, 2},
            };
            int[] orders = new int[] { 150, 200, 300 };

            for(int i = 0; i < nTypes; i++)
                model.AddGe(model.ScalProd(patterns.Select(p => p[i]).ToArray(), nForPatterns), orders[i]);

            model.AddMinimize(model.Sum(nForPatterns));
            
            if (model.Solve())
            {
                Console.Clear();
                Console.WriteLine("Total # of rolls = {0}", model.GetObjValue());
                var decisionValues = model.GetValues(nForPatterns);
                Console.WriteLine("Decisions = {0}, {1}, {2}, {3}, {4}, {5}",
                    decisionValues[0], decisionValues[1], decisionValues[2], decisionValues[3], decisionValues[4], decisionValues[5]);
            }
            else
            {
                Console.WriteLine("Failed to optimize LP.");
            }
        }
    }
}
