﻿using ILOG.Concert;
using ILOG.CPLEX;
using System;

namespace IE5001_CS
{
    // air pollution control
    class Example_5
    {
        static public void RunCplex()
        {
            Cplex model = new Cplex();
            INumVar[] decisions = model.NumVarArray(6, 0.0, 1.0); // decision variables  
            model.AddMinimize(model.ScalProd(new double[] { 8, 10, 7, 6, 11, 9 }, decisions)); // cost, objective         
            model.AddGe(model.ScalProd(new double[] { 12, 9, 25, 20, 17, 13 }, decisions), 60); // particulate, constraint
            model.AddGe(model.ScalProd(new double[] { 35, 42, 18, 31, 56, 49 }, decisions), 150); // sulfur oxides, constraint
            model.AddGe(model.ScalProd(new double[] { 37, 53, 28, 24, 29, 20 }, decisions), 125); // hydrocarbon, contraint            

            if (model.Solve())
            {
                Console.Clear();
                Console.WriteLine("Cost = {0}", model.GetObjValue());
                var decisionValues = model.GetValues(decisions);
                Console.WriteLine("Decisions = {0}, {1}, {2}, {3}, {4}, {5}", 
                    decisionValues[0], decisionValues[1], decisionValues[2], decisionValues[3], decisionValues[4], decisionValues[5]);
            }
            else
            {
                Console.WriteLine("Failed to optimize LP.");
            }
        }
    }
}
