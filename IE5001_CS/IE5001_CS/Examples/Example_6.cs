﻿using ILOG.Concert;
using ILOG.CPLEX;
using System;

namespace IE5001_CS
{
    class Example_6
    {
        static public void RunCplex()
        {
            Cplex model = new Cplex();
            INumVar[] decisions = model.NumVarArray(4, 0.0, double.PositiveInfinity); // decision variables     
            model.AddMinimize(model.ScalProd(new double[] { 800, 400, 600, 500 }, decisions)); // cost, objective         
            model.AddGe(model.ScalProd(new double[] { 10, 3, 8, 2 }, decisions), 5); // Element A
            model.AddGe(model.ScalProd(new double[] { 90, 150, 75, 175 }, decisions), 100); // Element B
            model.AddGe(model.ScalProd(new double[] { 45, 25, 20, 37 }, decisions), 30); // Element C
            model.AddEq(model.ScalProd(new double[] { 1, 1, 1, 1 }, decisions), 1); // Fraction

            if (model.Solve())
            {
                Console.Clear();
                Console.WriteLine("Cost = {0}", model.GetObjValue());
                var decisionValues = model.GetValues(decisions);
                Console.WriteLine("Decisions = {0}, {1}, {2}, {3}",
                    decisionValues[0], decisionValues[1], decisionValues[2], decisionValues[3]);
            }
            else
            {
                Console.WriteLine("Failed to optimize LP.");
            }
        }
    }
}
