﻿using System;

namespace IE5001_CS
{
    class Program
    {
        static void Main(string[] args)
        {
            //Example_1.RunCplex();
            Example_2.RunCplex();
            //Example_3.RunCplex();
            //Example_4.RunCplex();
            //Example_5.RunCplex();
            //Example_6.RunCplex();
            //Example_7.RunCplex();
            //Example_8.RunCplex();
            //Example_9.RunCplex();
            Console.WriteLine();

            //Example_2.RunMoCompass();
            //Console.WriteLine();

            //Example_2.RunMoCompass_LagrangianRelaxation();
            //Console.WriteLine();

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
